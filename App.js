/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import LoginMain from './screens/Login/LoginMain';
import ForgetEmail from './screens/Login/ForgetEmail';
import Register from './screens/Login/Register';
import OTP from './screens/Login/OTP';
import Profile from './screens/Login/Profile';

const MainNavigator = createStackNavigator({
  LoginMain: {screen: LoginMain},
  ForgetEmail: {screen: ForgetEmail},
  Register: {screen: Register},
  OTP: {screen: OTP},
  Profile: {screen: Profile},
});

const App = createAppContainer(MainNavigator);
export default App;
