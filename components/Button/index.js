import PropTypes from 'prop-types'
import React from 'react'
import {View, TouchableOpacity, Text} from 'react-native';

const noop = () => {}

const Button = ({
    onPress = noop,
    buttonStyles,
    textStyles,
    content
  }) => {
    return (
    <View style={buttonStyles}>
        <TouchableOpacity onPress={onPress}>
            <Text style={textStyles}>{content}</Text>
        </TouchableOpacity>
    </View>
    );
  }

  Button.defaultProps = {
    onPress: noop
  }
  
  Button.propTypes = {
    onPress: PropTypes.func,
    buttonStyles: PropTypes.any,
    textStyles: PropTypes.any,
    content: PropTypes.string
  }
  
  export default Button
  