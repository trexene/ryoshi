import React from 'react';
import {View, TouchableOpacity, Image, TextInput, Text} from 'react-native';
import s from './styles'
import Button from '../../../components/Button'
import {StyleSheet} from 'react-native'
import { whileStatement, inheritLeadingComments } from '@babel/types';

const styleSheet = StyleSheet.create({ 
    nextContainer:{
        marginTop:30,
        alignSelf:'center',
        flexDirection:'row', 
        alignContent: 'space-between'
    },
    input:{
        alignSelf:'center',
        color:'white',
        borderColor:'red',
        borderWidth:3,
        paddingHorizontal:55,
        marginVertical:20,
        paddingVertical:10,
        width: '90%',
        fontSize:24,
        borderRadius: 30
    },
    nameText:{
        color: 'white',
        fontSize: 38,
        fontWeight: 'bold',
        alignSelf: 'center',
        margin: 10
    },
});
class Profile extends React.Component {
    static navigationOptions = {
        title: 'PROFILE',
        headerStyle: {
            backgroundColor: 'red',
          },
        headerTintColor: '#fff',
        headerTitleStyle: {
        color: 'white',
        },
      };
      
      render() {
    const {navigate} = this.props.navigation;
    
    return (
        <View style={s.container}>
            <View style={styleSheet.nextContainer}>   
            <Image source={require('../../../images/icons/user-3.png')} style={{height:130, width:130, tintColor: 'white'}}></Image>     
            </View>
            <View>
                <Text style={styleSheet.nameText}>
                    Hello {this.props.navigation.getParam('name','')}!
                </Text>
            </View>
            <Text style = {styleSheet.input}>
                {/* RM {this.props.balance} */}
                RM 0.00
            </Text>
            <Image source={require('../../../images/icons/diamond.png')} style={{alignSelf:'flex-start',height:30, width:30, marginHorizontal:35,marginTop:-65, tintColor: 'white'}}></Image>     
            <Text style = {styleSheet.input}>
            {/* {this.props.points} Points */}
            0 Points
            </Text>
            <Image source={require('../../../images/icons/star-1.png')} style={{alignSelf:'flex-start',height:30, width:30, marginHorizontal:35,marginTop:-65, tintColor: 'white'}}></Image>   
        </View>
    )
      }
    }
    export default Profile;