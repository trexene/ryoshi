import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: 'black',
    alignItems:'center'
  },
  mainTitle:{
    fontSize:36,
    fontWeight:'normal',
    color:'white',
    padding: 20
  },
  secTitle:{
    fontWeight:'bold'
  },
  hr:{
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    width: 200,
    marginBottom: 20
  }
})


export default styles