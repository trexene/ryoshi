import React from 'react';
import { View, Picker, TextInput, Text } from 'react-native';
import s from './styles'
import Button from '../../../components/Button'
import { StyleSheet } from 'react-native'
import ValidationComponent from 'react-native-form-validator';
import { ScrollView } from 'react-native-gesture-handler';
import moment from "moment";
import DateTimePicker from '@react-native-community/datetimepicker';

const styleSheet = StyleSheet.create({
  button: {
    height: 50,
    width: '90%',
    backgroundColor: 'red',
    borderRadius: 30,
    justifyContent: 'center',
    marginVertical: 20,
    alignSelf: 'center'
  },
  text: {
    color: 'white',
    fontSize: 18,
    backgroundColor: 'red',
    alignSelf: 'center'
  },
  specialContact: {
    color: 'white',
    fontSize: 18,
    alignSelf:'flex-start',
    marginTop: -45,
    marginLeft: 45,
    marginBottom: 20
  },
  input: {
    alignSelf: 'center',
    color: 'white',
    borderColor: 'red',
    borderWidth: 3,
    paddingHorizontal: 25,
    marginVertical: 5,
    width: '90%',
    borderRadius: 30
  },
  inputContact: {
    alignSelf: 'center',
    color: 'white',
    borderColor: 'red',
    borderWidth: 3,
    paddingLeft: 50,
    marginVertical: 5,
    width: '90%',
    borderRadius: 30
  },
  inputDate: {
    alignSelf: 'center',
    color: 'white',
    borderColor: 'red',
    borderWidth: 3,
    paddingLeft: 25,
    marginVertical: 5,
    marginHorizontal: 5,
    width: '28%',
    borderRadius: 30
  },
  picker: {
    color: 'white',
    marginLeft: -10,
    width: '200%'
  }
});
class Register extends ValidationComponent {
  static navigationOptions = {
    headerShown: false,
  };
  state = {
    name: '',
    password: '',
    confirm_password: '',
    email: '',
    contact: '',
    pickerDate: new Date(),
    birthday: '',
    gender: '',
    matchErr: '',
    mode: 'date',
    show: false,
    year: 0,
    month: 0,
    day: 0,
    yearList: [
      2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, 2001, 2000, 1999, 1998, 1997, 1996, 1995, 1994, 1993, 1992, 1991, 1990, 1989, 1988, 1987, 1986, 1985, 1984, 1983, 1982, 1981, 1980, 1979, 1978, 1977, 1976, 1975, 1974, 1973, 1972, 1971, 1970, 1969, 1968, 1967, 1966, 1965, 1964, 1963, 1962, 1961, 1960, 1959, 1958, 1957, 1956, 1955, 1954, 1953, 1952, 1951, 1950, 1949, 1948, 1947, 1946, 1945, 1944, 1943, 1942, 1941, 1940, 1939, 1938, 1937, 1936, 1935, 1934, 1933, 1932, 1931, 1930, 1929, 1928, 1927, 1926, 1925, 1924, 1923, 1922, 1921, 1920, 1919, 1918, 1917, 1916, 1915, 1914, 1913, 1912, 1911, 1910, 1909, 1908, 1907, 1906, 1905, 1904, 1903, 1902, 1901
    ],
    monthList: [
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
    ],
    dayList: []

  }

  render() {

    let yearItems = this.state.yearList.map((s, i) => {
      return <Picker.Item key={i} value={s} label={s.toString()} />
    });
    let monthItems = this.state.monthList.map((s, i) => {
      return <Picker.Item key={i} value={s} label={s.toString()} />
    });
    let dayItems = this.state.dayList.map((s, i) => {
      return <Picker.Item key={i} value={s} label={s.toString()} />
    });
    const { navigate } = this.props.navigation;
    function IsJsonString(str) {
      let strTest= 
      '{"balance": 0, "contact": "6123123123232", "created_at": "2019-12-30 05:50:42", "dob": "2015-08-02 00:00:00", "email": "aaa@aaa.aaadw", "gender": "Male", "id": 17, "name": "aaa", "password": "aaa", "points": 0, "updated_at": "2019-12-30 05:50:42", "verify_code": "235772"}'
      try {
          JSON.parse(str);
      } catch (e) {
          return false;
      }
      return true;
  }
    return (
      <ScrollView style={{
        height: '100%',
        backgroundColor: 'black',
      }}>
        <View style={s.container}>
          <Text style={s.mainTitle}>CREATE <Text style={s.secTitle}>ACCOUNT</Text></Text>
          <View style={s.hr}></View>
          <TextInput ref="name" onChangeText={(name) => this.setState({ name })} value={this.state.name} placeholder="Your nickname" placeholderTextColor="white" style={styleSheet.input}>
          </TextInput>

          {this.isFieldInError('name') && this.getErrorsInField('name').map(errorMessage => <Text style={{ color: 'red' }}>{errorMessage}</Text>)}
          <TextInput ref="email" onChangeText={(email) => this.setState({ email })} value={this.state.email} placeholder="Your email" placeholderTextColor="white" style={styleSheet.input} onChangeText={(text) => { this.setState({ email: text }) }} >
          </TextInput>

          {this.isFieldInError('email') && this.getErrorsInField('email').map(errorMessage => <Text style={{ color: 'red' }}>{errorMessage}</Text>)}
          <TextInput ref="password" onChangeText={(password) => this.setState({ password })} value={this.state.password} secureTextEntry={true} placeholder="Your password" placeholderTextColor="white" style={styleSheet.input}>
          </TextInput>
          {this.isFieldInError('password') && this.getErrorsInField('password').map(errorMessage => <Text style={{ color: 'red' }}>{errorMessage}</Text>)}
          <TextInput ref="confirm_password" onChangeText={(confirm_password) => this.setState({ confirm_password })} value={this.state.confirm_password} secureTextEntry={true} placeholder="Confirm password" placeholderTextColor="white" style={styleSheet.input}>
          </TextInput>
          {this.state.matchErr !== '' &&
            <Text style={{ color: 'red' }}>{this.state.matchErr}</Text>
          }

          <Text style={{ color: 'white' }}>Date of Birth :</Text>
          <View style={{ flexDirection: 'row' }}>

            <View
              style={styleSheet.inputDate}>
              <Picker
                style={styleSheet.picker} selectedValue={this.state.year}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ year: itemValue }, () => {
                    if (this.state.year != 0 && this.state.month != 0) {
                      var numDay = new Date(this.state.year, this.state.month, 0).getDate();
                      var dayArr = [];

                      for (var i = 1; i <= numDay; i++) {
                          dayArr.push(i);
                      }
                      this.setState({ dayList: dayArr, day:0, birthday:'' })
                    }
                    else{
                      if(this.state.year != 0 && this.state.month != 0 && this.state.day != 0){
                        this.setState({ birthday: moment(new Date(this.state.year, this.state.month, this.state.day)).format('DD/MM/YYYY') })
                      } else{
                        this.setState({ birthday: '' })
                      }
                    }
                  })}>
                <Picker.Item key='' label="Year" value="" />
                {yearItems}
              </Picker>
            </View>
            {this.state.year != 0 &&
              <View
                style={styleSheet.inputDate}>
                <Picker
                  style={styleSheet.picker} selectedValue={this.state.month}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ month: itemValue }, () => {
                      if (this.state.year != 0 && this.state.month != 0) {
                        var numDay = new Date(this.state.year, this.state.month, 0).getDate();
                        var dayArr = [];

                        for (var i = 1; i <= numDay; i++) {
                            dayArr.push(i);
                        }
                        this.setState({ dayList: dayArr, day:0, birthday:'' })
                      }else{
                        if(this.state.year != 0 && this.state.month != 0 && this.state.day != 0){
                          this.setState({ birthday: moment(new Date(this.state.year, this.state.month, this.state.day)).format('DD/MM/YYYY') })
                        } else{
                          this.setState({ birthday: '' })
                        }
                      }
                    })}>
                  <Picker.Item key='' label="Month" value="" />
                  {monthItems}
                </Picker>
              </View>
            }

            {(this.state.year != 0 && this.state.month != 0) &&
              <View
                style={styleSheet.inputDate}>
                <Picker
                  style={styleSheet.picker} selectedValue={this.state.day}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ day: itemValue },()=>{
                      if(this.state.year != 0 && this.state.month != 0 && this.state.date != 0){
                        this.setState({ birthday: moment(new Date(this.state.year, this.state.month, this.state.day)).format('YYYY-MM-DD HH:mm:ss') })
                      } else{
                        this.setState({ birthday: '' })
                      }
                    })}>
                  <Picker.Item key='' label="Day" value="" />
                  {dayItems}
                </Picker>
              </View>
            }
          </View>

          {this.isFieldInError('birthday') && this.getErrorsInField('birthday').map(errorMessage => <Text style={{ color: 'red' }}>{errorMessage}</Text>)}
          
          <TextInput ref="contact" keyboardType={'numeric'} onChangeText={(contact) => this.setState({ contact })} value={this.state.contact} placeholder="Contact Number" placeholderTextColor="white"
            //onChangeText={(text) => { this.setState({ contact: text})}} 
            style={styleSheet.inputContact}>
          </TextInput>
          <Text style={styleSheet.specialContact}>+6</Text>
          {this.isFieldInError('contact') && this.getErrorsInField('contact').map(errorMessage => <Text style={{ color: 'red' }}>{errorMessage}</Text>)}
          <View
            style={styleSheet.input}>
            <Picker
              style={styleSheet.picker} selectedValue={this.state.gender}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ gender: itemValue })}>
                <Picker.Item key='' label="Gender" value="" />
              <Picker.Item key='M' label="Male" value="Male" />
              <Picker.Item key='F' label="Female" value="Female" />
            </Picker>
          </View>
          {this.isFieldInError('gender') && this.getErrorsInField('gender').map(errorMessage => <Text style={{ color: 'red' }}>{errorMessage}</Text>)}
          {this.state.regErr !== '' &&
            <Text style={{ color: 'red' }}>{this.state.regErr}</Text>
          }
          <Button key="register"
            onPress={
              () => {
                if (this.state.password !== this.state.confirm_password) {
                  this.setState({ matchErr: "Passwords does not match" });
                } else {
                  this.setState({ matchErr: "" });

                  if (this.validate({
                    name: { minlength: 3, required: true },
                    password: { minlength: 3, required: true },
                    confirm_password: { minlength: 3, required: true },
                    email: { email: true, required: true },
                    contact: { numbers: true, minlength: 9, maxlength: 12, required: true },
                    birthday: { required: true },
                    gender: { required: true },
                  })) {

                    //user creation
                    
                    var v_string = "";
                    var possible = "0123456789";

                    for (var i = 0; i < 6; i++)
                      v_string += possible.charAt(Math.floor(Math.random() * possible.length));

                    fetch('http://13.229.67.139/users', {
                      method: 'POST',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        name: this.state.name,
                        password: this.state.password,
                        email: this.state.email,
                        contact: "6"+this.state.contact,
                        gender: this.state.gender,
                        birthday: this.state.birthday,
                        verify_code: v_string,
                      }),
                    })
                            .then(response => response.json())
                      .then((response) => {
                        console.log(response);
                        if(response.created_at){
                          this.setState({ regErr: '' });

                    //otp request

                    var url = 'https://www.smss360.com/api/sendsms.php?email=' + this.state.email + '&key=6754f9376556c8bb2faa4302963ba032&recipient=' + this.state.contact + '&message=ryoshi.com.my\nRyoshi.com.my\nRYOSHI.COM\nRYOSHI IZAKAYA SIGNATURE Your account is successfully created, your verification code is ' + v_string;
                    // url="test whatever url"; 
                    fetch(url, {
                      method: 'GET'
                    })
                      .then((response) => console.log(response))
                      .catch((error) => {
                        console.error(error);
                      });
                        navigate('OTP', { email: this.state.email, contact: this.state.contact });
                        }
                        else{
                          this.setState({ regErr: 'Registration Failed. Duplicate Email or number' })
                        }
                      })
                      .catch((error) => {
                        console.error(error);
                      });
                  }
                  else
                    console.log(this.getErrorMessages());
                }
              }

            }
            buttonStyles={styleSheet.button}
            textStyles={styleSheet.text}
            content="SIGN UP">
          </Button>
        </View></ScrollView>
    )
  }
}
export default Register;