import React from 'react';
import {View, TouchableOpacity, Image, TextInput, Text} from 'react-native';
import s from './styles'
import Button from '../../../components/Button'
import {StyleSheet} from 'react-native'
import { whileStatement, inheritLeadingComments } from '@babel/types';

const styleSheet = StyleSheet.create({ 
    button:{
        height:50,
        width: '90%',
        backgroundColor:'red',
        borderRadius: 30,
        justifyContent: 'center',
        marginTop: 20,
        alignItems: 'center'
    },
    text:{
        color:'white', 
        fontSize: 18, 
        backgroundColor:'red',
    },
    text1:{
        color:'white', 
        fontSize: 24,
        fontWeight:'bold', 
    },
    text2:{
        color:'gray', 
        fontSize: 18,
        textAlign:'center',
        width: '80%'
    }
});
class ForgetEmail extends React.Component {
    static navigationOptions = {
        headerShown: false,
      };
      render() {
    const {navigate} = this.props.navigation;
    
    return (
        <View style={s.container}>
            <Image source={require('../../../images/forgetmail.png')} style={{height:300, width:300, marginVertical:20}}></Image>
            <Text style = {styleSheet.text1}>
                Check your email
            </Text>
            <Text style = {styleSheet.text2}>
                We've sent instructions on how to reset your password.
            </Text>
            <Button
            buttonStyles = {styleSheet.button} 
            textStyles = {styleSheet.text}
            content = "Go to to your email" 
            onPress={() => navigate('LoginMain', {name: 'Jane'})}>
            </Button>
        </View>
    )
      }
    }
    export default ForgetEmail;