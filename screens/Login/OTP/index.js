import React from 'react';
import {View, TouchableOpacity, Image, TextInput, Text} from 'react-native';
import s from './styles'
import Button from '../../../components/Button'
import {StyleSheet} from 'react-native'
import { whileStatement, inheritLeadingComments } from '@babel/types';

const styleSheet = StyleSheet.create({ 
    button:{
        backgroundColor:'red',
        borderRadius: 130,
        justifyContent: 'center',
        marginTop: 20,
        padding: 10,
        paddingVertical:5,
        alignItems: 'flex-end',
    },
    input:{
        alignSelf:'center',
        color:'white',
        borderColor:'red',
        borderWidth:3,
        paddingHorizontal:25,
        marginVertical:5,
        width: '90%',
        borderRadius: 30
    },
    text:{
        color:'white', 
        marginTop: -5,
        fontSize: 32, 
        borderRadius: 130,
        backgroundColor:'red',
    },
    text1:{
        color:'white', 
        fontWeight:'bold', 
        textAlign:'center',
        width: '80%'
    },
    text2:{
        marginTop:20,
        color:'white',
        textAlign:'center',
        width:'70%' 
    },
    resend:{
        color:'white', 
        textAlign:'left',
        width: '80%'
    },
    nextContainer:{
        marginRight:30,
        alignSelf:'flex-end',
        flexDirection:'row', 
        alignContent: 'space-between'
    }
});
class OTP extends React.Component {
    static navigationOptions = {
        headerShown: false,
      };

    state = {countdown: -1}
      render() {
    const {navigate,getParam} = this.props.navigation;
    
    return (
        <View style={s.container}>
            <Text style={styleSheet.text1}>VERIFICATION CODE</Text>
            <Image source={require('../../../images/icons/smartphone-7.png')} style={{alignSelf:'flex-start',height:300, width:300,  marginHorizontal:35,marginVertical:30, tintColor: 'white'}}></Image> 
            
            <TextInput  keyboardType={'numeric'} placeholder="Enter Verification Code" placeholderTextColor="white" style = {styleSheet.input}>
            </TextInput>
            { this.state.countdown<1 && 
            <TouchableOpacity 
                    onPress={() => {
                        console.log('check nav',getParam('email',''));
                        url = 'https://www.smss360.com/api/sendsms.php?email='+getParam('email', '')+'&key=6754f9376556c8bb2faa4302963ba032&recipient='+getParam('contact', '')+'&message=ryoshi.com.my\nRyoshi.com.my\nRYOSHI.COM\nRYOSHI%20IZAKAYA%20SIGNATURE\nYour account is successfully created, your verification code is 123456'
                        fetch(url)
                        .then((response) => console.log(response))
                        .catch((error) => {
                        console.error(error);
                        });

                        this.setState({countdown: 30});

                        setTimeout(() => {
                            this.setState({countdown: -1})
                        }, 30000);
                            
                    }}>
                    <Text style={styleSheet.resend}>
                        RESEND CODE >
                    </Text>
                </TouchableOpacity>
            }
            { this.state.countdown>0 && 
                    <Text style={styleSheet.resend}>
                        PLEASE WAIT AT LEAST {this.state.countdown} SECOND TO RESEND CODE
                    </Text>
            }
            <Text style = {styleSheet.text2}>
                We will send you ONE time SMS message. Carrier rates may apply.
            </Text>
            <View style={styleSheet.nextContainer}>   
                    <Button
                    buttonStyles = {styleSheet.button} 
                    textStyles = {styleSheet.text}
                    content = "&#10140;" 
                    onPress={() => navigate('LoginMain')}>
                    </Button>
            </View>
        </View>
    )
      }
    }
    export default OTP;