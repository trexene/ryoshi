import React from 'react';
import { View, TouchableOpacity, Image, TextInput, Text, } from 'react-native';
import s from './styles'
import Button from '../../../components/Button'
import { StyleSheet } from 'react-native'
import { whileStatement, inheritLeadingComments } from '@babel/types';
import { NavigationActions, StackActions } from 'react-navigation';

const styleSheet = StyleSheet.create({
    button: {
        height: 50,
        width: '90%',
        backgroundColor: 'red',
        borderRadius: 30,
        justifyContent: 'center',
        marginTop: 30,
        alignSelf: 'center'
    },
    text: {
        color: 'white',
        fontSize: 18,
        backgroundColor: 'red',
        alignSelf: 'center'
    },
    input: {
        alignSelf: 'center',
        color: 'white',
        borderColor: 'white',
        borderWidth: 3,
        paddingHorizontal: 55,
        marginVertical: 20,
        width: '90%',
        borderRadius: 30
    },
    linkText: {
        color: 'white'
    },
    linkContainer: {
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignContent: 'space-between'
    },
    inputContact: {
        alignSelf: 'center',
        color: 'white',
        borderColor: 'white',
        borderWidth: 3,
        paddingLeft: 105,
        marginVertical: 20,
        width: '90%',
        borderRadius: 30
    },
    specialContact: {
      color: 'white',
      fontSize: 18,
      alignSelf:'flex-start',
      marginTop: -60,
      marginLeft: 80,
      marginBottom: 50
    },
    imgContainer: {
        alignSelf: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start'
    }
});
class LoginMain extends React.Component {
    static navigationOptions = {
        headerShown: false,
        errLogin: ''
    };
    state = { email: '', password: '',errLogin:'',errForget:'' }
    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={s.container}>
                <Image source={require('../../../images/logo.png')} style={{ alignSelf: 'center', height: 306, width: 472, marginBottom: -50 }}></Image>
                <TextInput placeholder="Contact" placeholderTextColor="white" style={styleSheet.inputContact} onChangeText={(contact) => this.setState({ contact })} >
                </TextInput>

                <Text style={styleSheet.specialContact}>+6</Text>
                <Image source={require('../../../images/icons/user-3.png')} style={{ alignSelf: 'flex-start', height: 30, width: 30, marginHorizontal: 35, marginTop: -76, tintColor: 'white' }}></Image>
                {this.state.errForget !== '' &&
                    <Text style={{ color: 'red', alignSelf:'center', marginTop:20 }}>{this.state.errForget}</Text>
                }
                <TextInput secureTextEntry={true} placeholder="PASSWORD" placeholderTextColor="white" style={styleSheet.input} onChangeText={(password) => this.setState({ password })} >
                </TextInput>
                <Image source={require('../../../images/icons/locked-4.png')} style={{ alignSelf: 'flex-start', height: 30, width: 30, marginHorizontal: 35, marginTop: -62, tintColor: 'white' }}></Image>
                {this.state.errLogin !== '' &&
                    <Text style={{ color: 'red', alignSelf:'center', marginTop:20 }}>{this.state.errLogin}</Text>
                }
                <Button
                    buttonStyles={styleSheet.button}
                    textStyles={styleSheet.text}
                    content="SIGN IN"
                    onPress={() => {
                        fetch('http://13.229.67.139/users/login', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                contact: '6'+this.state.contact,
                                password: this.state.password,
                            }),
                        })
                            .then(response => response.json())
                            .then(response => {
                                if (!response.status) {
                                    this.setState({ errLogin: "" });
                                    console.log(response);
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [NavigationActions.navigate({ routeName: 'Profile', params: { name: response.name,point:response.point,balance:response.balance } })],
                                    });
                                    this.props.navigation.dispatch(resetAction);
                                }
                                else {
                                    this.setState({ errLogin: "Authentication error, please try again" });
                                    console.log(response)
                                }
                            })
                            .catch((error) => {
                                this.setState({ errLogin: error });
                            });
                    }}>
                </Button>
                <View style={styleSheet.linkContainer}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() => navigate('Register')}>
                        <Text style={styleSheet.linkText}>
                            Register
                    </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }}
                        onPress={() => {
                            fetch('http://13.229.67.139/users/forget', {
                                method: 'POST',
                                headers: {
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    contact: '6'+this.state.contact,
                                }),
                            })
                            .then(response => response.json())
                                .then(response => {
                                    console.log(response);
                                    if (response.status === "success") {
                                        this.setState({ errLogin: "" });
                                        navigate('ForgetEmail');
                                        console.log(response);
                                    }
                                    else {
                                        this.setState({ errLogin: "Number does not exist" });
                                        console.log(response)
                                    }
                                })
                                .catch((error) => {
                                    console.error(error);
                                });
                        }}>
                        <Text style={styleSheet.linkText}>
                            Forgot Password
                    </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
export default LoginMain;